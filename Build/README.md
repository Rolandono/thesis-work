Run Build/March-of-48.exe

Adding new languages:
    Add new folder to Build/Assests/Resources/Languages
    Must have a menu.json with the given format (checked by validator)
    And game folders

Adding new games:
    Add new folder to a language folder
    Must have a text.json with the given format (checked by validator)
    Characters in game/Characters
    Background images in game/Backgrounds