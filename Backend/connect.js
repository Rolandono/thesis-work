const logger = require('./Logger');
const myClient = require('./MyClient');
new myClient();
const client = myClient.Get();
client.connect();
const getHandler = require('./GetHandler');
const postHandler = require('./PostHandler');
const express = require('express');
const app = express();
const port = 3000;
app.set('port', port);
app.use(express.json());

app.get('/', (req, res) => {
    logger.getLog(req.body, req.query);
    if(!req.query.TASK){
        res.status(200).send({status:-1});
        return;
    }
    switch(req.query.TASK){
        case "GetAgeGroups":
            getHandler.getAgeGroups(client, req.query, res);
            break;
        // case "STOP":
        //     client.end();
        //     process.exit();
        case "GetGlobalScores":
            getHandler.getGlobalScores(client, req.query, res);
            break;
        default:
            res.status(200).send({status:-1});
            break;
    }
});

app.post('/', (req, res) => {
    logger.postLog(req.body, req.query);
    switch(req.body.task){
        case 'cre_user'://just right
            postHandler.createUser(client, req.body, res);
            break;

        case 'validateUser':
            postHandler.validateUser(client, req.body, res);
            break;

        case 'cre_questionaire':
            postHandler.createQuestionaire(client, req.body, res);
            break;

        case 'get_scores_by_user':
            postHandler.getScoresByUser(client, req.body, res);
            break;

        case 'add_suggestion':
            postHandler.addSuggestion(client, req.body, res);
            break;

        default:
            res.status(200).send({status:-1});
            break;
    };    
})

app.listen(port, () => {
    console.log('Server is running... on '+port);
})