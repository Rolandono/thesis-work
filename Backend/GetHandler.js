module.exports  = class GetHandler{
    static getAgeGroups(client, json, res){
        client.query('SELECT * FROM age', (err, result) => {
            if(err){
                console.log(err);
                res.status(400).send(err);
                return;
            }
            res.status(200).send(result.rows);            
        });
    }

    static getGlobalScores(client, json, res){
        let idArray = json.id;
        for(let i=0;i<json.id.length;i++){
            idArray[i] = [idArray[i]];
        }
        client.query('SELECT * from get_global_scores($1)',[idArray], (err, result) => {
            if(err){
                console.log(err);
                res.status(400).send(err);
                return;
            }
            console.log(result.rows);
            res.status(200).send(result.rows);
        })
    }
};