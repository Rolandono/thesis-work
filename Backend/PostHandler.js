module.exports  = class PostHandler{
    static createUser(client, json, res){
        client.query('select create_user($1, $2)', [json.name, json.age], (err, result) => {
            if(err){
                console.log(err);
                res.status(400).send(err);
                return;
            }
            console.log(result.rows);
            res.status(200).send(result.rows);
        });  
        console.log("finished");
    }

    static validateUser(client, json, res){
        client.query('select validate_user($1)', [json.id], (err, result) => {
            if(err){
                console.log(err);
                res.status(400).send(err);
                return;
            }
            console.log(result.rows);
            res.status(200).send(result.rows);
        });  
    }

    static createQuestionaire(client, json, res){
        client.query('select create_questionnaire($1, $2, $3)', [json.userKey,json.testID,json.option], (err, result) => {
            if(err){
                console.log(err);
                res.status(400).send(err);
                return;
            }
            console.log(result.rows);
            res.status(200).send(result.rows);
        });
    }

    static getScoresByUser(client, json, res){
        client.query('select * from get_scores_by_user($1)',[json.userKey], (err,result) => {
            if(err){
                console.log(err);
                res.status(400).send(err);
                return;
            }
            console.log(result.rows);
            res.status(200).send(result.rows);
        });
    }

    static addSuggestion(client, json, res){
        client.query('select * from add_suggestion($1,$2)',[json.userKey, json.text], (err, result) => {
            if(err){
                console.log(err);
                res.status(400).send(err);
                return;
            }
            console.log(result.rows);
            res.status(200).send(result.rows);
        });
    }
};