const fs = require('fs');
const options = {encoding:'utf-8', flag:'a+'};

module.exports = class Logger{
    static getLog(body, query){
        console.log('------GET REQUEST------');
        console.log(body);
        console.log(query);
        fs.writeFile('getLog.log', "\n\nTime: "+new Date().toLocaleString('hu-HU'), options, function(err){});
        fs.writeFile('getLog.log', "\nBody: " + JSON.stringify(body), options, function(err){});
        fs.writeFile('getLog.log', "\nQuery: " +JSON.stringify(query), options, function(err){});
    }

    static postLog(body, query){
        console.log('-------POST REQUEST--------');
        console.log(body);
        console.log(query);
        fs.writeFile('postLog.log', "\n\nTime: "+new Date().toLocaleString('hu-HU'), options, function(err){});
        fs.writeFile('postLog.log', "\nBody: " + JSON.stringify(body), options, function(err){});
        fs.writeFile('postLog.log', "\nQuery: " +JSON.stringify(query), options, function(err){});
    }
}