using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Helper class for animating the images
public class ImageHandler : MonoBehaviour
{
    public IEnumerator ImageHandlerSwitch(string name, Image img, bool left)
    {
        //all the possible animations, also defined in the JSONValidator
        switch (name)
        {
            case "shakeH":
                StartCoroutine(ShakeHorizontally(img));
                break;
            case "shakeV":
                StartCoroutine(ShakeVertically(img));
                break;
            case "scaleUp":
                StartCoroutine(ScaleUp(img));
                break;
            case "popUp":
                StartCoroutine(PopUp(img));
                break;
            case "showUp":
                StartCoroutine(ShowUp(img));
                break;
            case "slideIn":
                StartCoroutine(SlideIn(img, left));
                break;
            case "slideOut":
                StartCoroutine(SlideOut(img, left));
                break;
            case "FASOut":
                StartCoroutine(FadeAndSlideOut(img,left));
                break;
            case "FASIn":
                StartCoroutine(FadeAndSlideIn(img, left));
                break;
            default:
                break;
        }
        yield return null;
    }
    public static IEnumerator ShakeHorizontally(Image img)
    {
        Vector3 start = img.transform.position;
        float shakeValue = 40f;
        float shakeSpeed = 550f;
        
        for(int i = 0; i<2; i++)
        {
            while(img.transform.position.x < start.x + shakeValue)
            {
                img.transform.position += new Vector3(Time.deltaTime * shakeSpeed, 0, 0);
                yield return null;
            }

            while (img.transform.position.x > start.x - shakeValue)
            {
                img.transform.position -= new Vector3(Time.deltaTime * shakeSpeed, 0, 0);
                yield return null;
            }

            while (img.transform.position.x < start.x)
            {
                img.transform.position += new Vector3(Time.deltaTime * shakeSpeed, 0, 0);
                yield return null;
            }
        }
    }
    public IEnumerator ShakeVertically(Image img)
    {
        Vector3 start = img.transform.position;
        float shakeValue = 40f;
        float shakeSpeed = 600f;
        
        for(int i = 0; i<2; i++)
        {
            while(img.transform.position.y < start.y + shakeValue)
            {
                img.transform.position += new Vector3(0, Time.deltaTime * shakeSpeed, 0);
                yield return null;
            }

            while (img.transform.position.y > start.y - shakeValue)
            {
                img.transform.position -= new Vector3(0, Time.deltaTime * shakeSpeed, 0);
                yield return null;
            }

            while (img.transform.position.y < start.y)
            {
                img.transform.position += new Vector3(0, Time.deltaTime * shakeSpeed, 0);
                yield return null;
            }
        }
    }

    public IEnumerator ScaleUp(Image img)
    {
        Vector3 start = img.transform.localScale;
        float scaleValue = 1.4f;
        float scaleSpeed = 3f;
        for(int i = 0; i<2; i++)
        {
            while (img.transform.localScale.x < scaleValue)
            {
                img.transform.localScale += new Vector3(scaleValue * Time.deltaTime * scaleSpeed, scaleValue * Time.deltaTime * scaleSpeed, 0);
                yield return null;
            }
            while (img.transform.localScale.x > 1)
            {
                img.transform.localScale -= new Vector3(scaleValue * Time.deltaTime * scaleSpeed, scaleValue * Time.deltaTime * scaleSpeed, 0);
                yield return null;
            }
        }        
    }

    public IEnumerator PopUp(Image img)
    {
        float scaleValue = 1.4f;
        float scaleSpeed = 2.8f;

        img.transform.localScale = new Vector3(0, 0, 1);
        while(img.transform.localScale.x < scaleValue)
        {
            img.transform.localScale += new Vector3(Time.deltaTime * scaleSpeed, Time.deltaTime * scaleSpeed, 0);
            yield return null;
        }
        while (img.transform.localScale.x > 1)
        {
            img.transform.localScale -= new Vector3(Time.deltaTime * scaleSpeed, Time.deltaTime * scaleSpeed, 0);
            yield return null;
        }
    }

    public IEnumerator ShowUp(Image img)
    {
        float scaleSpeed = 2f;

        img.transform.localScale = new Vector3(0, 0, 1);
        while (img.transform.localScale.x < 1)
        {
            img.transform.localScale += new Vector3(Time.deltaTime * scaleSpeed, Time.deltaTime * scaleSpeed, 0);
            yield return null;
        }
    }

    public IEnumerator SlideIn(Image img, bool left)
    {
        Vector3 start = img.transform.position;
        float slideValue = 700f;
        float slideSpeed = 400f;

        if (left)
        {
            img.transform.position = start - new Vector3(slideValue, 0, 0);
            while(img.transform.position.x < start.x)
            {
                img.transform.position += new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                yield return null;
            }
        }
        else
        {
            img.transform.position = start + new Vector3(slideValue, 0, 0);
            while (img.transform.position.x > start.x)
            {
                img.transform.position -= new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                yield return null;
            }
        }
    }

    public IEnumerator SlideOut(Image img, bool left)
    {
        Vector3 start = img.transform.position;
        float slideValue = 700f;
        float slideSpeed = 400f;
        float traveledDistance = 0f;

        if (left)
        {
            while (traveledDistance < slideValue)
            {
                traveledDistance += Time.deltaTime * slideSpeed;
                img.transform.position -= new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                yield return null;
            }
        }
        else
        {
            while (traveledDistance < slideValue)
            {
                traveledDistance += Time.deltaTime * slideSpeed;
                img.transform.position += new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                yield return null;
            }
        }
    }

    public IEnumerator FadeAndSlideOut(Image img, bool left)
    {
        Vector3 start = img.transform.position;
        float slideValue = 700f;
        float slideSpeed = 400f;
        float fadeSpeed = 1f;
        float traveledDistance = 0f;

        if (left)
        {
            while (traveledDistance < slideValue)
            {
                traveledDistance += Time.deltaTime * slideSpeed;
                Debug.Log(traveledDistance);
                img.transform.position -= new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                img.GetComponent<Image>().color 
                    -= new Color(0,0,0,Time.deltaTime * fadeSpeed);
                yield return null;
            }
        }
        else
        {
            while (traveledDistance < slideValue)
            {
                traveledDistance += Time.deltaTime * slideSpeed;
                Debug.Log(traveledDistance);
                img.transform.position += new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                img.GetComponent<Image>().color
                    -= new Color(0,0,0,Time.deltaTime * fadeSpeed);
                yield return null;
            }
        }
    }

    public IEnumerator FadeAndSlideIn(Image img, bool left)
    {
        Vector3 start = img.transform.position;
        float slideValue = 700f;
        float slideSpeed = 400f;
        float fadeSpeed = 0.5f;

        img.GetComponent<Image>().color = new Color(255, 255, 255, 0);

        if (left)
        {
            img.transform.position -= new Vector3(slideValue, 0, 0);
            while (img.transform.position.x < start.x)
            {
                img.transform.position += new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                img.GetComponent<Image>().color
                    += new Color(0, 0, 0, Time.deltaTime * fadeSpeed);
                yield return null;
            }
            img.GetComponent<Image>().color
                    = new Color(255, 255, 255, 255);
        }
        else
        {
            img.transform.position += new Vector3(slideValue, 0, 0);
            while (img.transform.position.x > start.x)
            {
                img.transform.position -= new Vector3(Time.deltaTime * slideSpeed, 0, 0);
                img.GetComponent<Image>().color
                    += new Color(0, 0, 0, Time.deltaTime * fadeSpeed);
                yield return null;
            }
            img.GetComponent<Image>().color
                    = new Color(255, 255, 255, 255);
        }
    }
}
