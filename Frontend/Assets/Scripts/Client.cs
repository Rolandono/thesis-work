using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

//Client for communication with the backend
public class Client
{
    private HttpClient client;

    public Client()
    {
        client = new HttpClient();
    }

    //Returns the data for the scores page after a test
    public async Task<JArray> getGlobalScores(JToken test)
    {
        try
        {
            //adding parameters from the test
            string url = "http://rolandono.ath.cx:3000?TASK=GetGlobalScores&";
            for(int i = 0; i < ((JArray)test["test"]).Count; i++)
            {
                url += "id["+i+"]="+test["test"][i]["testID"]+"&";
            }


            HttpResponseMessage response = await client.GetAsync(url);
            var json = await response.Content.ReadAsStringAsync();
            var ja = JArray.Parse(json);
            return ja;
        }
        catch
        {
            return null;
        }
    }

    //Getting the datas from the questions stored, by user ID
    public async Task<JArray> getScoresByUser(int ID)
    {
        HttpResponseMessage response;
        var obj = new
        {
            task = "get_scores_by_user",
            userKey = ID
        };
        var jsonContent = JsonConvert.SerializeObject(obj);
        var content = new StringContent(jsonContent.ToString(), Encoding.UTF8, "application/json");
        JArray ja;
        try
        {
            response = await client.PostAsync("http://rolandono.ath.cx:3000/", content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            ja = JArray.Parse(jsonResponse);
            if (ja.Count == 0) throw new Exception();
        }
        catch
        {
            ja = JArray.Parse("[{\"s_time\":\"\",\"s_question\":\"No data\",\"s_answer\":\"Or no connection\",\"s_correct\":false}]");
        }
        return ja;
    }

    //Checking for the user on start
    public async Task<bool> ValidateUser(int ID)
    {
        bool result;
        HttpResponseMessage response;
        var obj = new
        {
            task = "validateUser",
            id = ID
        };
        var jsonContent = JsonConvert.SerializeObject(obj);
        var content = new StringContent(jsonContent.ToString(), Encoding.UTF8, "application/json");
        
        try
        {
            response = await client.PostAsync("http://rolandono.ath.cx:3000/", content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            var ja = JArray.Parse(jsonResponse);

            result = (bool)ja[0]["validate_user"];
            if (!result)
            {
                result = await CreateUser(PlayerPrefs.GetString("Name"), PlayerPrefs.GetInt("Age"));
            }
            return result;
        }
        catch
        {   
            return true;
        }
    }

    //Registering a user
    public async Task<bool> CreateUser(string nick, int ageID)
    {
        var obj = new
        {
            task = "cre_user",
            name = nick,
            //Backend serial starts from 1
            //While the value from the dropdown starts from 0
            age = ageID + 1
        };
        var jsonContent = JsonConvert.SerializeObject(obj);
        var content = new StringContent(jsonContent.ToString(), Encoding.UTF8, "application/json");
        
        try{
            var response = await client.PostAsync("http://rolandono.ath.cx:3000/", content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            var ja = JArray.Parse(jsonResponse);
            PlayerPrefs.SetInt("ID", (int)ja[0]["create_user"]);
            return true;
        }
        catch
        {
            //saved anyway locally
            return true;
        }
    }

    //Saving an answer after a question
    public async void SaveTest(int option, int testID)
    {
        var obj = new
        {
            task = "cre_questionaire",
            testID = testID,
            option = option,
            userKey = PlayerPrefs.GetInt("ID")
        };
        var jsonContent = JsonConvert.SerializeObject(obj);
        var content = new StringContent(jsonContent.ToString(), Encoding.UTF8, "application/json");
        try
        {
            var response = await client.PostAsync("http://rolandono.ath.cx:3000/", content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            var ja = JArray.Parse(jsonResponse);
            if ((bool)ja[0]["create_questionnaire"]) { }
            else { }
        }
        catch
        {
        }
    }

    //Sending back all the suggestions
    public async Task<bool> AddSuggestion(int userID, string suggestion)
    {
        var obj = new
        {
            task = "add_suggestion",
            userKey = userID,
            text = suggestion
        };
        var jsonContent = JsonConvert.SerializeObject(obj);
        var content = new StringContent(jsonContent.ToString(), Encoding.UTF8, "application/json");
        try
        {
            var response = await client.PostAsync("http://rolandono.ath.cx:3000/", content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            var jo = JObject.Parse(jsonResponse);
            if ((bool)jo["add_suggestion"]) { }
            else { }
        }
        catch
        {
        }
        return true;
    }





}
