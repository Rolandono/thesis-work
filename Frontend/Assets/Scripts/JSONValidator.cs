using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Helper class with functions to validate the Game-defining JSONs
public static class JSONValidator
{  
    //checking the content and the format of the lng/menu.json
    public static string MenuValidator(string txt)
    {
        JObject jo;
        string[] topLevelParameters = new string[] {"registration","menu"};
        string[] registrationParameters = new string[] { "head", "nick", "nickLabel", "age", "button" };
        string[] menuParameters = new string[] { "newGame", "continue", "extras", "scores", "suggestion","suggestionHead","suggestionPlaceholder","suggestionButtonText","exit" };

        //checking parsability
        try { 
            jo = JObject.Parse(txt);
        }
        catch {
            return "Fatal Error: not a JSON format in menu.json";
        }
        foreach (string topLevelParameter in topLevelParameters)
        {
            if (!jo.ContainsKey(topLevelParameter)) {
                return "Error: \"" + topLevelParameter + "\" is required in menu.json";
            }
        }
        foreach (string registrationParameter in registrationParameters)
        {
            if (!((JObject)jo["registration"]).ContainsKey(registrationParameter))
            {
                return "Error: \"" + registrationParameter + "\" is required in menu.json";
            }
        }
        foreach(string menuParameter in menuParameters)
        {
            if (!((JObject)jo["menu"]).ContainsKey(menuParameter))
            {
                return "Error: " + menuParameter + " is required in menu.json";
            }
        }
        return "";
    }

    //checking the content and the format of the lng/game/text.json
    public static string GameValidator(string txt)
    {
        JArray ja;
        string[] textParameters = new string[] { "id", "type", "speaker", "text", "charl", "charr", "animationl", "animationr", "next", "background" };
        string[] animations = new string[] {"shakeH", "shakeV", "scaleUp", "popUp", "showUp", "slideIn", "slideOut", "FASOut", "FASIn" };
        
        try
        {
            ja = JArray.Parse(txt);
        }
        catch
        {
            return "Error: text.json isn't a valid JSON Array";
        }
        for(int i = 0; i < ja.Count; i++)
        {
            if (!((JObject)ja[i]).ContainsKey("type"))
            {
                return "Error: " + i + "th element doesn't have a type";
            }
            if ((string)ja[i]["type"] != "text" && (string)ja[i]["type"] != "test")
            {
                return "Error: " + i + "th element's type isn't one of the following: (test, text)";
            }
            //text keys
            if ((string)ja[i]["type"] == "text")
            {
                //checking keys
                foreach(string textParameter in textParameters)
                {
                    if (!((JObject)ja[i]).ContainsKey(textParameter))
                    {
                        return "Error: " + i + "th element doesn't have a \""+textParameter+"\"";
                    }
                }
                //checking values
                if(ja[i]["id"].Type != JTokenType.Integer)
                {
                    return "Error: " + i + "th element's \"id\" isn't a valid integer";
                }
                if(ja[i]["background"].Type != JTokenType.String && ja[i]["charl"].Type != JTokenType.Null)
                {
                    return "Error: " + i + "th element's \"background\" isn't a valid string or null";
                }
                if (ja[i]["speaker"].Type != JTokenType.String)
                {
                    return "Error: " + i + "th element's \"speaker\" isn't a valid string";
                }
                if (((string)ja[i]["speaker"]).Length > 25)
                {
                    return "Error: " + i + "th element's \"speaker\" is longer than 25 characters";
                }
                if (ja[i]["text"].Type != JTokenType.String)
                {
                    return "Error: " + i + "th element's \"text\" isn't a valid string";
                }
                if (((string)ja[i]["text"]).Length > 180)
                {
                    return "Error: " + i + "th element's \"text\" is longer than 180 characters";
                }
                if (ja[i]["charl"].Type != JTokenType.String && ja[i]["charl"].Type != JTokenType.Null)
                {
                    return "Error: " + i + "th element's \"charl\" isn't a valid string or is not null";
                }
                if (ja[i]["charr"].Type != JTokenType.String && ja[i]["charr"].Type != JTokenType.Null)
                {
                    return "Error: " + i + "th element's \"charr\" isn't a valid string or is not null";
                }
                if (ja[i]["animationl"].Type != JTokenType.String && ja[i]["animationl"].Type != JTokenType.Null)
                {
                    return "Error: " + i + "th element's \"animationl\" isn't a valid string and is not null";
                }
                if (Array.IndexOf(animations, (string)ja[i]["animationl"]) < 0 && ja[i]["animationl"].Type != JTokenType.Null)
                {
                    return "Error: " + i + "th element's \"animationl\" isn't an existing type of animation";
                }
                if (ja[i]["animationr"].Type != JTokenType.String && ja[i]["animationr"].Type != JTokenType.Null)
                {
                    return "Error: " + i + "th element's \"animationr\" isn't a valid string and is not null";
                }
                if (Array.IndexOf(animations, (string)ja[i]["animationr"]) < 0 && ja[i]["animationr"].Type != JTokenType.Null)
                {
                    return "Error: " + i + "th element's \"animationr\" isn't an existing type of animation";
                }
                if (ja[i]["next"].Type != JTokenType.Integer)
                {
                    return "Error: " + i + "th element's \"next\" isn't a valid integer";
                }
            }
            //test keys
            else {
                if (ja[i]["id"].Type != JTokenType.Integer)
                {
                    return "Error: " + i + "th element's \"id\" isn't a valid integer";
                }
                if (ja[i]["title"].Type != JTokenType.String)
                {
                    return "Error: " + i + "th element's \"title\" isn't a valid integer";
                }
                if (((string)ja[i]["title"]).Length > 40)
                {
                    return "Error: " + i + "th element's \"title\" is longer than 40 characters";
                }
                if (ja[i]["next"].Type != JTokenType.Integer)
                {
                    return "Error: " + i + "th element's \"next\" isn't a valid integer";
                }
                if(ja[i]["test"].Type != JTokenType.Array)
                {
                    return "Error: " + i + "th element's \"next\" isn't a valid JSON Array";
                }
                if(((JArray)ja[i]["test"]).Count != 4)
                {
                    return "Error: " + i + "th element's \"test\" must have exactly 4 elements";
                }
                for(int j = 0; j < 4; j++)
                {
                    if (!((JObject)ja[i]["test"][j]).ContainsKey("question"))
                    {
                        return "Error: " + i + "th element's "+j+"th test doesn't have a question";
                    }
                    if (((string)ja[i]["test"][j]["question"]).Length > 100)
                    {
                        return "Error: " + i + "th element's " + j + "th test's question is longer than 100";
                    }
                    if (!((JObject)ja[i]["test"][j]).ContainsKey("options"))
                    {
                        return "Error: " + i + "th element's " + j + "th test doesn't have a options";
                    }
                    if (((JArray)ja[i]["test"][j]["options"]).Count != 4)
                    {
                        return "Error: " + i + "th element's " + j + "th test must have 4 options";
                    }
                    for(int k = 0; k< 4; k++)
                    {
                        if (((string)ja[i]["test"][j]["options"][k]).Length > 40)
                        {
                            return "Error: " + i + "th element's " + j + "th test's "+k+"th option is longer than 40";
                        }
                    }
                }
            }

        }
        return "";
    }

}
