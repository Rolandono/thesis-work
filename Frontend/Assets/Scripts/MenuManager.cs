using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.IO;
using System;
using UnityEngine.UI;
using System.Threading;
using UnityEngine.SceneManagement;

//The logic behind the 1st scene (menu, uservalidation)
public class MenuManager : MonoBehaviour
{
    //saved values
    private int ID;
    private List<string> Languages;
    private string Language;
    private List<string> Games;
    private string Game;
    private List<GameObject> scoreList;

    //JSONs
    private JObject menuJSON;

    //misc
    public Client client;
    public StreamReader r;
    public Image cover;
    public static int LoadID = 0;

    //UI components
    public GameObject pnlRegister;
    public GameObject pnlMenu;
    public GameObject pnlSuggestions;
    public TMP_InputField inpName;
    public TMP_Dropdown drpAge;
    public TMP_Dropdown drpGame;
    public TMP_Dropdown drpLanguage;

    //UI Registration Text Components
    public TMP_Text txtHead;
    public TMP_Text txtNick;
    public TMP_Text txtNickPlaceHolder;
    public TMP_Text txtAge;
    public TMP_Text txtButton;

    //UI Suggestions
    public TMP_Text txtSuggestionHead;
    public TMP_Text txtSuggestionButtonText;
    public TMP_Text txtSuggestionPlaceHolder;
    public TMP_InputField inpSuggestion;

    //UI Menu Text Components
    public TMP_Text txtNewGame;
    public TMP_Text txtContinue;
    public TMP_Text txtExtras;
    public TMP_Text txtScores;
    public TMP_Text txtSuggestion;   
    public TMP_Text txtExit;

    //UI error
    public GameObject pnlError;
    public TMP_Text txtError;

    //SVGs
    public Sprite correct;
    public Sprite wrong;

    //UI scores
    public GameObject pnlScores;
    public GameObject pnlScore;
    public GameObject pnlContent;

    //UI extras text
    public TMP_Text txtMirrored;
    public TMP_Text txtCharacter;
    public TMP_Text txtBG;
    public TMP_Text txtPosition;

    //Setup the Client, runs before Start
    void Awake()
    {
        client = new Client();
    }

    // Start is called before the first frame update, uservalidation
    async void Start()
    {
        ID = PlayerPrefs.GetInt("ID", 0);
        if (ID == 0 && PlayerPrefs.GetString("Name").Length == 0)
        {
            //If never registered
            pnlRegister.SetActive(true);
            fillRegistration();
            fillLanguageOptions();
            fillGameOptions();
        }
        else
        {
            if(await client.ValidateUser(ID)){
                //if user has registered and valid or offline
                pnlMenu.SetActive(true);
                fillMenu();
                fillLanguageOptions();
                fillGameOptions();
            }
            else
            {
                //if offline without name (only in earlier versions)
                pnlRegister.SetActive(true);
                fillRegistration();
                fillLanguageOptions();
                fillGameOptions();
            }
        }
        //Removing cover, showing the menu or registration
        StartCoroutine(hideCover());
    }

    /***************************************
                 EVENT LISTENERS
     ***************************************/

    //Saving after the registration
    public async void onBtnSave()
    {
        PlayerPrefs.SetInt("Age", drpAge.value);
        PlayerPrefs.SetString("Name", inpName.text);
        if (await client.CreateUser(inpName.text, drpAge.value))
        {
            pnlRegister.SetActive(false);
            pnlMenu.SetActive(true);
        }
    }

    //Send back the user ID and the suggestion
    public async void onAddSuggestion()
    {
        await client.AddSuggestion(PlayerPrefs.GetInt("ID", 0), inpSuggestion.text);
        pnlSuggestions.SetActive(false);
        inpSuggestion.text = "";
    }

    //Continue from a save
    public void onBtnContinue()
    {
        LoadID = PlayerPrefs.GetInt("Save" + Language + Game, 0);
        StartCoroutine(StartNewGame());
    }

    //Leave game
    public void onBtnExit()
    {
        Application.Quit();
    }

    //Changing the langauge of the menu (and possible games)
    public void onDrpLanguageChange(int value)
    {
        StartCoroutine(ChangeLanguage());
    }

    //Changing the game
    public void onDrpGameChange(int value)
    {
        PlayerPrefs.SetString("Game", Games[drpGame.value]);
    }

    //Starting a new game
    public void onNewGame()
    {
        StartCoroutine(StartNewGame());
    }

    //Showing the scores view
    public async void onBtnScores()
    {
        await showScoresAsync();
    }

    //Closing the scores view
    public void onBtnCloseScores()
    {
        //Destroying all the score objects
        foreach (GameObject line in scoreList)
        {
            Destroy(line.gameObject);
        }
        pnlScores.SetActive(false);
    }

    //Show the suggestion form
    public void onBtnSuggestion()
    {
        pnlSuggestions.SetActive(true);
    }

    //Hide the suggestion form
    public void onBtnSuggestionClose()
    {
        pnlSuggestions.SetActive(false);
    }

    /***************************************
                 TEXT DISPLAYING
     ***************************************/

    //Loading the menu texts
    public void fillMenu()
    {
        string validationResult;
        string txt;
        Language = PlayerPrefs.GetString("Language", "HU");
        Game = PlayerPrefs.GetString("Game", "March-of-48");

        //Validation after reading
        try
        {
            r = new StreamReader("Assets/Resources/Languages/" + Language + "/menu.json");
        }
        catch (Exception e)
        {
            showError(e.Message);
        }        
        txt = r.ReadToEnd();
        validationResult = JSONValidator.MenuValidator(txt);
        if(validationResult != "")
        {
            showError(validationResult);
        }

        //If valid show the texts
        else
        {
            menuJSON = JObject.Parse(txt);
            txtNewGame.text = (string)menuJSON["menu"]["newGame"];
            txtContinue.text = (string)menuJSON["menu"]["continue"];
            txtExtras.text = (string)menuJSON["menu"]["extras"];
            txtScores.text = (string)menuJSON["menu"]["scores"];
            txtSuggestion.text = (string)menuJSON["menu"]["suggestion"];
            txtSuggestionHead.text = (string)menuJSON["menu"]["suggestionHead"];
            txtSuggestionPlaceHolder.text = (string)menuJSON["menu"]["suggestionPlaceholder"];
            txtSuggestionButtonText.text = (string)menuJSON["menu"]["suggestionButtonText"];
            txtMirrored.text = (string)menuJSON["menu"]["mirrored"];
            txtCharacter.text = (string)menuJSON["menu"]["character"];
            txtBG.text = (string)menuJSON["menu"]["background"];
            txtPosition.text = (string)menuJSON["menu"]["position"];
            txtExit.text = (string)menuJSON["menu"]["exit"];
        }
    }

    //Setting up the registration window, depending on the language
    public void fillRegistration()
    {
        string txt;
        string validationString;
        Language = PlayerPrefs.GetString("Language", "HU");
        Game = PlayerPrefs.GetString("Game", "March-of-48");
        try
        {
            r = new StreamReader("Assets/Resources/Languages/" + Language + "/menu.json");
        }
        catch (Exception e)
        {
            showError(e.Message);
        }
        txt = r.ReadToEnd();
        validationString = JSONValidator.MenuValidator(txt);
        if(validationString != "")
        {
            showError(validationString);
        }
        else
        {
            menuJSON = JObject.Parse(txt);
            txtHead.text = (string)menuJSON["registration"]["head"];
            txtNick.text = (string)menuJSON["registration"]["nick"];
            txtAge.text = (string)menuJSON["registration"]["age"];
            txtButton.text = (string)menuJSON["registration"]["button"];
            txtNickPlaceHolder.text = (string)menuJSON["registration"]["nickLabel"];
        }
    }

    /***************************************
                 LOADING OPTIONS
     ***************************************/

    //Filling the dropdown, with possible languages
    public void fillLanguageOptions()
    {
        Languages = new List<string>();
        string[] folders = Directory.GetDirectories("Assets\\Resources\\Languages", "*", SearchOption.TopDirectoryOnly);
        List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
        TMP_Dropdown.OptionData optionData;
        drpLanguage.ClearOptions();
        foreach (string folder in folders)
        {
            string name = folder.Split('\\')[folder.Split('\\').Length - 1];
            Languages.Add(name);
            optionData = new TMP_Dropdown.OptionData();
            optionData.text = name;
            options.Add(optionData);
        }
        drpLanguage.AddOptions(options);
        if (Languages.Contains(PlayerPrefs.GetString("Language","HU")))
        {
            drpLanguage.value = Languages.IndexOf(PlayerPrefs.GetString("Language", "HU"));
        }
        else
        {
            drpLanguage.value = 1;
        }
    }

    //Filling the dropdown with possible games
    public void fillGameOptions()
    {
        Games = new List<string>();
        string[] folders = Directory.GetDirectories("Assets\\Resources\\Languages\\"+ PlayerPrefs.GetString("Language","HU"), "*", SearchOption.TopDirectoryOnly);
        List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
        TMP_Dropdown.OptionData optionData;
        drpGame.ClearOptions();
        foreach (string folder in folders)
        {
            string name = folder.Split('\\')[folder.Split('\\').Length - 1];
            Games.Add(name);
            optionData = new TMP_Dropdown.OptionData();
            optionData.text = name;
            options.Add(optionData);
        }
        drpGame.AddOptions(options);
        if (Games.Contains(PlayerPrefs.GetString("Game", "March-of-48")))
        {
            drpGame.value = Games.IndexOf("March-of-48") + 1;
        }
        else
        {
            drpGame.value = 1;
        }
    }

    /***************************************
                   SCORES VIEW
     ***************************************/

    //Displaying all the questions and answers by a user, ordered by date
    public async Task showScoresAsync()
    {
        //Fetching the data
        JArray scores = await client.getScoresByUser(ID);

        //Resizing the the content to be able to scroll it
        pnlContent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 100 * scores.Count + 50);
        scoreList = new List<GameObject>();

        for(int i = 0; i < scores.Count; i++)
        {
            //Copy the schema
            GameObject score = Instantiate(pnlScore, pnlContent.transform);
            score.gameObject.SetActive(true);

            //Setting up the position and the text
            score.transform.position -= new Vector3(0, i * 70);
            score.transform.Find("TxtTime").GetComponent<TMP_Text>().text = (string)scores[i]["s_time"];
            score.transform.Find("TxtQuestion").GetComponent<TMP_Text>().text = (string)scores[i]["s_question"];
            score.transform.Find("TxtAnswer").GetComponent<TMP_Text>().text = (string)scores[i]["s_answer"];

            //Different sign for different answers
            if((bool)scores[i]["s_correct"] == true)
            score.transform.Find("SVG").GetComponent<Unity.VectorGraphics.SVGImage>().sprite = correct;
            else
            score.transform.Find("SVG").GetComponent<Unity.VectorGraphics.SVGImage>().sprite = wrong;

            //Adding all the lines to a list, to destroy them when the view is closed
            scoreList.Add(score);
        }
        pnlScores.SetActive(true);
    }

    /***************************************
                 HELPER FUNCTIONS
     ***************************************/

    //Displaying errors
    public void showError(string text = "Something went wrong")
    {
        pnlError.SetActive(true);
        txtError.text = text;
    }

    //Hiding the black cover
    public IEnumerator hideCover(float multiplier = 0.2f)
    {
        float t = 0;
        while (t < 1)
        {
            cover.color = Color.Lerp(Color.black, new Color(0,0,0,0), t);
            t += multiplier * Time.deltaTime;
            yield return null;
        }
        cover.gameObject.SetActive(false);
    }

    //Showing the black cover
    public IEnumerator showCover(float multiplier = 0.2f)
    {
        cover.gameObject.SetActive(true);
        float t = 0;
        while (t < 1)
        {
            cover.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, t);
            t += multiplier * Time.deltaTime;
            yield return null;
        }
    }

    //Eventlistener for the langauge dropdown
    public IEnumerator ChangeLanguage()
    {
        yield return StartCoroutine(showCover(0.5f));
        pnlError.SetActive(false);
        PlayerPrefs.SetString("Language", Languages[drpLanguage.value]);
        try { fillMenu(); } catch { }
        try { fillRegistration(); } catch { }
        fillGameOptions();
        yield return StartCoroutine(hideCover(0.5f));
    }

    //Starting a new game
    public IEnumerator StartNewGame()
    {
        yield return StartCoroutine(showCover(0.5f));
        SceneManager.LoadScene(1);
    }
}
