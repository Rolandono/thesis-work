using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static ImageHandler;

//The logic behind the 2nd scene: game, tests, scores and displaying any errors
public class NovelManager : MonoBehaviour, IPointerClickHandler
{
    //globals
    private string language;
    private string game;
    private string state; 
    private JArray json;
    private int next;
    private Client client;
    private bool TextSlowerRunning = false;
    private UnityEngine.Coroutine textSlowerCR;
    private UnityEngine.Coroutine autoModeCR;
    private UnityEngine.Coroutine skipModeCR;
    private UnityEngine.Coroutine leftAnimationCR;
    private UnityEngine.Coroutine rightAnimationCR;
    private string actualText;
    private bool autoMode = false;
    private bool skipMode = false;
    private List<GameObject> historyList;


    //UI components
    public Canvas canvas;

    //game elements
    public GameObject pnlText;
    public GameObject pnlScores;
    public GameObject[] scores;
    public GameObject clickCatcher;
    public TextMeshProUGUI speech;
    public TextMeshProUGUI speaker;
    public Image imgLeft;
    public Image imgRight;
    public Image imgBG;
    public Sprite defSprite;
    public Vector3 leftStartVec;
    public Vector3 rightStartVec;
    private ImageHandler imgHandler;

    //cover
    public Image cover;

    //test elements
    private ArrayList answers;
    public GameObject pnlTest;
    public TMP_Text txtTestTitle;
    public TMP_Text txtTestQuestion;
    public Button[] btnOptions;
    public JToken testToken;
    public int testNext;

    //error components
    public GameObject pnlError;
    public TMP_Text txtError;

    //textBox buttons
    public TMP_Text txtAuto;
    public TMP_Text txtSkip;

    //History view
    public GameObject pnlHistory;
    public GameObject historyLine;
    public GameObject pnlHistoryContent;

    //runs only once, when the game started/loaded
    void Start()
    {
        //setting up the global variables
        StreamReader sr;
        imgHandler = GetComponent<ImageHandler>();
        client = new Client();
        state = "game";
        language = PlayerPrefs.GetString("Language", "HU");
        game = PlayerPrefs.GetString("Game", "March-of-48");
        leftStartVec = imgLeft.transform.position;
        rightStartVec = imgRight.transform.position;
        try
        {
            //reading the game controlling text.json
            sr = new StreamReader("Assets/Resources/Languages/"+language+"/"+game+"/text.json");
            string txt = sr.ReadToEnd();

            //validating text.json
            string validatorResponse = JSONValidator.GameValidator(txt);
            if (validatorResponse != "")
            {
                state = "error";
                showError(validatorResponse);
            }
            else
            {
                //global variables
                json = JArray.Parse(txt);
                pnlText.SetActive(true);
                cover.gameObject.SetActive(true);                

                //starting a new game or continuing
                if (MenuManager.LoadID != 0)
                {
                    next = MenuManager.LoadID;
                }
                else
                {
                    next = (int)json[0]["id"];
                }

                //paging forward
                StartCoroutine(hideCover());
                PageForward();
            }
        }
        catch(Exception e)
        {
            showError(e.Message);
        }        
    }  

    /***************************************
                  3 MAIN PAGES
     ***************************************/

    //Paging, loading the page by the JSON
    public void PageForward()
    {
        //Exit on end
        if (next < 0)
        {
            StartCoroutine(leaveNovel());
            try { StopCoroutine(autoModeCR); } catch { }
            autoMode = false;
            txtAuto.text = "Auto";
            try { StopCoroutine(skipModeCR); } catch { }
            skipMode = false;
            txtSkip.text = "Skip";
            return;
        }

        //Load the next page
        foreach (var item in json)
        {
            //Find the next one, might not be in order
            if((int)item["id"] == next)
            {
                //Immediately create a save
                PlayerPrefs.SetInt("Save" + language + game, (int)item["id"]);

                //If test is up next, load it
                if((string)item["type"] == "test")
                {
                    try { StopCoroutine(autoModeCR); } catch { }
                    autoMode = false;
                    txtAuto.text = "Auto";
                    try { StopCoroutine(skipModeCR); } catch { }
                    skipMode = false;
                    txtSkip.text = "Skip";
                    testToken = item;
                    StartCoroutine(LoadTest());
                    return;
                }

                //Otherwise, load all the content for the next page

                //Stop the image animations
                try { StopCoroutine(rightAnimationCR); } catch { }
                imgRight.transform.position = rightStartVec;
                try { StopCoroutine(leftAnimationCR); } catch { }
                imgLeft.transform.position = leftStartVec;

                //Texts
                speaker.text = item["speaker"].ToString();
                next = (int)item["next"];
                speech.text = "";
                actualText = (string)item["text"];                

                //Display the background
                if ((string)item["background"] == null)
                {
                    imgBG.GetComponent<Image>().sprite = defSprite;
                }
                else
                {
                    Sprite sp = Resources.Load<Sprite>("Languages/" + language + "/" + game + "/Backgrounds/" + (string)item["background"]);
                    imgBG.GetComponent<Image>().sprite = sp;
                }

                //Left character, animation
                if ((string)item["charl"] == null)
                {
                    imgLeft.GetComponent<Image>().color = new Color(0,0,0,0);
                }
                else
                {
                    float difference;
                    Sprite sp = Resources.Load<Sprite>("Languages/" + language + "/" + game + "/Characters/"+(string)item["charl"]);
                    difference = sp.rect.width / sp.rect.height;                    
                    imgLeft.transform.rotation = Quaternion.Euler(0, 180, 0);                                      
                    imgLeft.rectTransform.sizeDelta =
                        new Vector2(imgLeft.rectTransform.rect.height * difference, imgLeft.rectTransform.sizeDelta.y);
                    imgLeft.GetComponent<Image>().sprite = sp;
                    imgLeft.GetComponent<Image>().color = Color.white;
                }

                //Right character, animation
                if ((string)item["charr"] == null)
                {
                    imgRight.GetComponent<Image>().color = new Color(0, 0, 0, 0);
                }
                else
                {
                    float difference;
                    Sprite sp = Resources.Load<Sprite>("Languages/" + language + "/" + game + "/Characters/" + (string)item["charr"]);
                    difference = sp.rect.width / sp.rect.height;     
                    imgRight.rectTransform.sizeDelta =
                        new Vector2(imgRight.rectTransform.rect.height * difference, imgRight.rectTransform.sizeDelta.y);
                    imgRight.GetComponent<Image>().sprite = sp;
                    imgRight.GetComponent<Image>().color = Color.white;
                }

                //Storing the animation coroutines, might have to stop them later
                leftAnimationCR = StartCoroutine(imgHandler.ImageHandlerSwitch((string)item["animationl"],imgLeft, true));
                rightAnimationCR = StartCoroutine(imgHandler.ImageHandlerSwitch((string)item["animationr"],imgRight, false));
                textSlowerCR = StartCoroutine(TextSlower(0.07f, item["text"].ToString()));
                return;
            }
        }

        //If no coresponding page found, show error
        state = "error";
        showError("No page found with ID: " + next);
        return;
    }

    //Loading the next test
    public void SwapTestText()
    {
        //Leave the test at the end(scores or back to game) 
        if(testNext == ((JArray)testToken["test"]).Count)
        {
            StartCoroutine(UnLoadTest());
            return;
        }

        //Setting up the question
        int testID = (int)testToken["test"][testNext]["testID"];
        txtTestQuestion.text = (string)testToken["test"][testNext]["question"];

        //Setting up the answers
        for (int i = 0; i < 4; i++)
        {
            int option = i;
            Button btn = btnOptions[i];
            btn.GetComponentInChildren<TMP_Text>().text = (string)testToken["test"][testNext]["options"][i];
            if (i == (int)testToken["test"][testNext]["solution"])
            {
                btn.onClick.AddListener(() => {
                    StartCoroutine(ChoosenOption(btn, true, option, testID));
                });
            }
            else
            {
                btn.onClick.AddListener(() => {
                    StartCoroutine(ChoosenOption(btn, false, option, testID));
                });
            }
        }

        //Prepare for the next test
        testNext++;
    }

    //Loading the score after a test, if available
    public IEnumerator LoadScores()
    {
        JArray ja = null;

        //Getting the global scores to display
        if (language == "HU" && game == "March-of-48")
        {
            var task = client.getGlobalScores(testToken);
            yield return new WaitUntil(() => task.IsCompleted);
            ja = task.Result;
        }
        //Leave if not in the right game or if the backend is not available (back to game)
        if (ja is null)
        {
            state = "game";
            pnlText.SetActive(true);
            PageForward();
        }
        //Load the global scores
        else
        {            
            pnlScores.SetActive(true);
            for (int i = 0; i < ja.Count; i++)
            {
                string title = (string)ja[i]["q_text"];
                float correct = (float)ja[i]["q_right"];
                float wrong = (float)ja[i]["q_wrong"];
                scores[i].transform.Find("txtQuestionTitle").GetComponent<TMP_Text>().text = (string)ja[i]["q_text"];
                scores[i].transform.Find("txtCorrectNum").GetComponent<TMP_Text>().text
                    = correct + " (" + Mathf.RoundToInt(correct / (correct + wrong) * 100) + "%)";
                scores[i].transform.Find("txtWrongNum").GetComponent<TMP_Text>().text
                    = wrong + " (" + Mathf.RoundToInt(wrong / (correct + wrong) * 100) + "%)";
                if ((bool)answers[i])
                    scores[i].transform.Find("txtAnswer").GetComponent<TMP_Text>().text = "Helyes";
                else
                    scores[i].transform.Find("txtAnswer").GetComponent<TMP_Text>().text = "Helytelen";
            }
            state = "scores";
        }
    }


    /***************************************
                   ERRORS
     ***************************************/

    //Displaying error messages
    public void showError(string txt)
    {
        pnlError.SetActive(true);
        txtError.text = txt;
    }

    //Close the game, whenever an error shows up
    public void onBtnExit()
    {
        Application.Quit();
    }

    /***************************************
           LOADER, UNLOADER FUNCTIONS
     ***************************************/
    //Setting up variables before the test, runs once before every test
    public IEnumerator LoadTest()
    {
        testNext = 0;
        answers = new ArrayList();
        state = "test";
        yield return showCover();
        pnlText.SetActive(false);
        pnlTest.SetActive(true);
        txtTestTitle.text = (string)testToken["title"];
        SwapTestText();
        next = (int)testToken["next"];
        yield return hideCover();
    }

    //Setting up the continuation after the test (scores or game or another test)
    public IEnumerator UnLoadTest()
    {
        yield return showCover();
        pnlTest.SetActive(false);
        next = (int)testToken["next"];
        StartCoroutine(LoadScores());
        yield return hideCover();
        //clickCatcher.SetActive(true);
    }    

    //Setting up the continuation after the scores (game or another test)
    public IEnumerator UnLoadScores()
    {
        state = "test";
        yield return StartCoroutine(showCover());
        pnlScores.SetActive(false);
        pnlText.SetActive(true);
        PageForward();
        yield return StartCoroutine(hideCover());
        state = "game";
    }

    //Slowly remove the black cover
    public IEnumerator hideCover(float multiplier = 0.2f)
    {
        float t = 0;
        while (t < 1)
        {
            cover.color = Color.Lerp(Color.black, new Color(0, 0, 0, 0), t);
            t += multiplier * Time.deltaTime;
            yield return null;
        }
        cover.gameObject.SetActive(false);
    }

    //Slowly show the black cover
    public IEnumerator showCover(float multiplier = 0.2f)
    {
        cover.gameObject.SetActive(true);
        float t = 0;
        while (t < 1)
        {
            cover.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, t);
            t += multiplier * Time.deltaTime;
            yield return null;
        }
    }

    //Show the cover, go back to menu
    public IEnumerator leaveNovel()
    {
        yield return StartCoroutine (showCover());
        SceneManager.LoadScene(0);
    }

    /***************************************
                   TEST LOGIC
     ***************************************/

    //The logic behind the test buttons
    //If in the right game, store the answer
    //Display the result
    public IEnumerator ChoosenOption(Button sender, bool right, int option, int testID)
    {
        float t = 0;
        Color color;

        //Storing the result for the scores page
        answers.Add(right);

        //Remove the listeners
        foreach(Button b in btnOptions)
        {
            b.onClick.RemoveAllListeners();
        }

        //Store the answers if possible
        if(game == "March-of-48" && language == "HU")
        {
            client.SaveTest(option, testID);
        }

        //Display the result
        if (right) { color = Color.green; }
        else { color = Color.red; }
        for(int i = 0; i<3; i++)
        {
            while (t < 1)
            {
                sender.gameObject.GetComponent<Image>().color = Color.Lerp(Color.white, color, t);
                t += 2f * Time.deltaTime;
                yield return null;
            }
            t = 1;
            while(t > 0)
            {
                sender.gameObject.GetComponent<Image>().color = Color.Lerp(Color.white, color, t);
                t -= 2f * Time.deltaTime;
                yield return null;                
            }
            t = 0;
        }

        //Setup the next question
        SwapTestText();
    }

    /***************************************
                 TEXT DISPLAYING
     ***************************************/

    //Showing the text char by char
    public IEnumerator TextSlower(float time, string text)
    {
        TextSlowerRunning = true;
        foreach (char ch in text)
        {
            speech.text += ch;
            yield return new WaitForSeconds(time);
        }
        TextSlowerRunning = false;
        StopCoroutine(TextSlower(0.0f, ""));
    }

    //Slowly breezing through the text without interactions
    public IEnumerator AutoMode(float seconds)
    {
        while (autoMode)
        {
            StopCoroutine(textSlowerCR);
            TextSlowerRunning=false;
            PageForward();
            yield return new WaitForSeconds(seconds);
        }
    }

    //Quickly paging through the texts
    public IEnumerator SkipMode(float seconds)
    {
        while (skipMode)
        {
            PageForward();
            StopCoroutine(textSlowerCR);
            TextSlowerRunning = false;
            speech.text = actualText;
            yield return new WaitForSeconds(seconds);
        }
    }

    /***************************************
                 EVENT LISTENERS
     ***************************************/

    //Go back to the menu
    public void onReturn()
    {
        StartCoroutine(leaveNovel());
    }

    //Showing the current page again
    public void onRepeat()
    {
        //Stop the text
        StopCoroutine(textSlowerCR);
        TextSlowerRunning = false;

        //Find the current id and page to that
        foreach (var item in json)
        {
            if ((int)item["next"] == next)
            {
                next = (int)item["id"];
                break;
            }
        }
        PageForward();
    }

    //Starting auto mode
    public void onAuto()
    {
        //Stop the skipping
        try { StopCoroutine(skipModeCR); } catch { }
        skipMode = false;
        txtSkip.text = "Skip";

        //If in auto, stop
        autoMode = !autoMode;
        if (autoMode)
        {
            StopCoroutine(textSlowerCR);
            TextSlowerRunning = false;
            txtAuto.text = "Stop";
            autoModeCR = StartCoroutine(AutoMode(500));
        }
        //Otherwise start auto
        else
        {
            txtAuto.text = "Auto";
            StopCoroutine(autoModeCR);
        }
    }

    //Starting skip mode
    public void onSkip()
    {
        //stop auto mode
        try { StopCoroutine(autoModeCR); } catch { }
        autoMode = false;
        txtAuto.text = "Auto";
        skipMode = !skipMode;

        //If in skip mode, stop it
        if (skipMode)
        {
            StopCoroutine(textSlowerCR);
            TextSlowerRunning = false;
            txtSkip.text = "Stop";
            skipModeCR = StartCoroutine(SkipMode(0.3f));
        }
        //Otherwise, start it
        else
        {
            txtSkip.text = "Skip";
            StopCoroutine(skipModeCR);
        }
    }

    //Showing the history of pages, up to 15
    public void onHistory()
    {
        //making sure, no paging is allowed during this view
        state = "history";

        historyList = new List<GameObject>();

        //Iterating backwards
        int last = next;
        bool match = false;

        //Searching for the last 15 pages
        for (int i = 0; i<15; i++)
        {
            foreach (var item in json)
            {
                //Find the next one, might not be in order
                if ((int)item["next"] == last)
                {
                    //Not counting inbetween tests
                    if((string)item["type"] == "test")
                    {
                        match = true;
                        last = (int)item["id"];
                        i--;
                        break;
                    }

                    //Copying the schema
                    GameObject newLine = Instantiate(historyLine, pnlHistoryContent.transform);

                    //Adding the texts, setting up the position
                    newLine.name = "HistoryLine";
                    newLine.transform.position -= new Vector3(0, i * 100);
                    newLine.transform.Find("TxtHisSpeaker").GetComponent<TMP_Text>().text = (string)item["speaker"];
                    newLine.transform.Find("TxtHisSpeech").GetComponent<TMP_Text>().text = (string)item["text"];
                    newLine.GetComponent<Button>().onClick.AddListener(delegate { onHistoryLine((int)item["id"]); });
                    newLine.gameObject.SetActive(true);

                    //Storing the lines, to be able to delete them later
                    historyList.Add(newLine);

                    //No match on the 1st, stop there
                    match = true;
                    last = (int)item["id"];
                    break;
                }
            }
            //Stop on the 1st page
            if (!match)
            {
                break;
            }
            //Otherwise go on
            else
            {
                match = false;
            }
        }
        //Resizing content view
        pnlHistoryContent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 140 * historyList.Count + 20);
        pnlHistory.SetActive(true);
    }

    //Closing the content view
    public void onCloseHistory()
    {
        //Delete all the lines
        foreach (GameObject line in historyList)
        {
            Destroy(line.gameObject);
        }
        pnlHistory.SetActive(false);
        state = "game";
    }

    //Jump backwards to the selected page
    public void onHistoryLine(int id)
    {
        next = id;

        //Delete all the lines
        foreach(GameObject line in historyList)
        {
            Destroy(line.gameObject);
        }
        pnlHistory.SetActive(false);

        //Stop the text displaying, and then page
        StopCoroutine(textSlowerCR);
        TextSlowerRunning = false;
        state = "game";
        PageForward();
    }

    /***************************************
                  CLICK LISTENER
     ***************************************/

    //Listening for click throughout the scene
    //Avoiding unnecessary listening in Update
    public void OnPointerClick(PointerEventData eventData)
    {
        //Different responses, depending on the displayed content
        switch (state)
        {
            //In "game", display instantly the text, or page
            case "game":
                if (TextSlowerRunning)
                {
                    StopCoroutine(textSlowerCR);
                    TextSlowerRunning = false;
                    speech.text = actualText;
                }
                else
                {
                    PageForward();
                }
                break;

            //Do noting in test
            case "test":
                break;

            //Do nothing when error is displayed
            case "error":
                break;

            //Do nothing when history is displayed
            case "history":
                break;

            //Leave on 1st click in scores
            case "scores":
                StartCoroutine(UnLoadScores());
                break;

            //Otherwise do nothing
            default:
                break;
        }

    }
}
