using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

//The logic behind the extras view, at the menu
public class ExtrasHandler : MonoBehaviour
{
    //Main panel
    public GameObject pnlExtras;

    public GameObject pnlMenu;

    //Images
    public Image BG;
    public Image ImgLeft;
    public Image ImgCenter;
    public Image ImgRight;

    //Counter for buttons
    private int BGCounter;
    private int CharCounter;
    private int PositionCounter;
    private bool mirrored;

    //List of the available files
    List<string> chars;
    List<string> BGs;

    //Preapring the page, setting up the values, reading the files
    public void onBtnExtras()
    {
        pnlMenu.SetActive(false);

        BGCounter = 0;
        CharCounter = 0;
        PositionCounter = 0;
        mirrored = false;

        //getting the characters
        chars = new List<string>();
        foreach (string fileName in Directory.GetFiles("Assets\\Resources\\Languages\\"
                            + PlayerPrefs.GetString("Language", "HU") + "\\"
                            + PlayerPrefs.GetString("Game", "March-of-48")
                            + "\\Characters", "*jpg"))
        {
            string fileN = fileName.Substring(17, fileName.Length - 21);            
            chars.Add(fileN);
        }
        foreach (string fileName in Directory.GetFiles("Assets\\Resources\\Languages\\"
                            + PlayerPrefs.GetString("Language", "HU") + "\\"
                            + PlayerPrefs.GetString("Game", "March-of-48")
                            + "\\Characters", "*png"))
        {
            string fileN = fileName.Substring(17, fileName.Length - 21);
            chars.Add(fileN);
        }

        //getting the backgrounds
        BGs = new List<string>();
        foreach (string fileName in Directory.GetFiles("Assets\\Resources\\Languages\\"
                            + PlayerPrefs.GetString("Language", "HU") + "\\"
                            + PlayerPrefs.GetString("Game", "March-of-48")
                            + "\\Backgrounds", "*jpg"))
        {
            string fileN = fileName.Substring(17, fileName.Length - 21);
            BGs.Add(fileN);
        }
        foreach (string fileName in Directory.GetFiles("Assets\\Resources\\Languages\\"
                            + PlayerPrefs.GetString("Language", "HU") + "\\"
                            + PlayerPrefs.GetString("Game", "March-of-48")
                            + "\\Backgrounds", "*png"))
        {
            string fileN = fileName.Substring(17, fileName.Length - 21);
            BGs.Add(fileN);
        }

        pnlExtras.SetActive(true);

        //finally, setting up the 1st page
        LoadImage();
    }

    //rerendering the content
    public void LoadImage()
    {
        float difference;

        ImgLeft.gameObject.SetActive(false);
        ImgCenter.gameObject.SetActive(false);
        ImgRight.gameObject.SetActive(false);

        //first, load the background
        Sprite sp = Resources.Load<Sprite>(BGs[BGCounter % BGs.Count]);
        BG.GetComponent<Image>().sprite = sp;

        //then load the character
        sp = Resources.Load<Sprite>(chars[CharCounter % chars.Count]);   
        
        //there are 3 different position with 3 different images
        switch (PositionCounter % 3)
        {
            case 0:
                //resizing the image depending on the source file
                difference = sp.rect.width / sp.rect.height;
                ImgLeft.rectTransform.sizeDelta = 
                    new Vector2(ImgLeft.rectTransform.rect.height * difference, ImgLeft.rectTransform.sizeDelta.y);
                ImgLeft.GetComponent<Image>().sprite = sp;   
                
                //mirroring the image
                if(mirrored) ImgLeft.transform.rotation = Quaternion.Euler(0, 180, 0);
                else ImgLeft.transform.rotation = Quaternion.Euler(0, 0, 0);

                //displaying
                ImgLeft.gameObject.SetActive(true);
                break;
            case 1:
                //resizing the image depending on the source file
                difference = sp.rect.width / sp.rect.height;
                ImgCenter.rectTransform.sizeDelta =
                    new Vector2(ImgCenter.rectTransform.rect.height * difference, ImgCenter.rectTransform.sizeDelta.y);
                ImgCenter.GetComponent<Image>().sprite = sp;

                //mirroring the image
                if (mirrored) ImgCenter.transform.rotation = Quaternion.Euler(0, 180, 0);
                else ImgCenter.transform.rotation = Quaternion.Euler(0, 0, 0);

                //displaying
                ImgCenter.gameObject.SetActive(true);
                break;
            case 2:
                //resizing the image depending on the source file
                difference = sp.rect.width / sp.rect.height;
                ImgRight.rectTransform.sizeDelta =
                    new Vector2(ImgRight.rectTransform.rect.height * difference, ImgRight.rectTransform.sizeDelta.y);
                ImgRight.GetComponent<Image>().sprite = sp;

                //mirroring the image
                if (mirrored) ImgCenter.transform.rotation = Quaternion.Euler(0, 180, 0);
                else ImgCenter.transform.rotation = Quaternion.Euler(0, 0, 0);

                //displaying
                ImgRight.gameObject.SetActive(true);
                break;

            default:
                break;
        }
    }

    /***************************************
                 EVENT LISTENERS
     ***************************************/

    public void onBtnLeftBG()
    {
        if (BGCounter != 0) BGCounter--;
        else BGCounter = BGs.Count - 1;
        LoadImage();
    }

    public void onBtnRightBG()
    {
        BGCounter++;
        LoadImage();
    }

    public void onBtnLeftChar()
    {
        if (CharCounter != 0) CharCounter--;
        else CharCounter = chars.Count - 1;
        LoadImage();
    }

    public void onBtnRightChar()
    {
        CharCounter++;
        LoadImage();
    }

    public void onBtnLeftPosition()
    {
        if (PositionCounter != 0) PositionCounter--;
        else PositionCounter = 2;
        LoadImage();
    }

    public void onBtnRightPosition()
    {
        PositionCounter++;
        LoadImage();
    }

    public void onBtnClose()
    {
        pnlExtras.SetActive(false);
        pnlMenu.SetActive(true);
    }

    public void onRotateChar()
    {
        mirrored = !mirrored;
        LoadImage();
    }
}
