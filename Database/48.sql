CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "nickname" varchar,
  "created_at" date,
  "age_id" int,
  "user_number" int unique
);

CREATE TABLE "age" (
  "id" SERIAL PRIMARY KEY,
  "group" varchar
);

CREATE TABLE "questions" (
  "id" SERIAL PRIMARY KEY,
  "text" varchar
);

CREATE TABLE "options" (
  "id" SERIAL PRIMARY KEY,
  "text" varchar,
  "question_id" int,
  "correct" bool,
  "option_id" int
);

CREATE TABLE "questionnaire" (
  "id" SERIAL PRIMARY KEY,
  "taken_at" timestamp,
  "user_id" int,
  "question_id" int,
  "option_id" int
);

create table "suggestions" (
  "id" SERIAL PRIMARY KEY,
  "taken_at" timestamp,
  "user_id" int,
  "text" varchar
)

ALTER TABLE "users" ADD FOREIGN KEY ("age_id") REFERENCES "age" ("id");

ALTER TABLE "options" ADD FOREIGN KEY ("question_id") REFERENCES "questions" ("id");

ALTER TABLE "questionnaire" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "questionnaire" ADD FOREIGN KEY ("question_id") REFERENCES "questions" ("id");

ALTER TABLE "questionnaire" ADD FOREIGN KEY ("option_id") REFERENCES "options" ("id");

alter table "suggestions" add foreign key ("user_id") references "users"("id");

insert into age(id, "group") values (1,'0-14');
insert into age(id, "group") values (2,'15-18');
insert into age(id, "group") values (3,'19-25');
insert into age(id, "group") values (4,'26-35');
insert into age(id, "group") values (5,'36-45');
insert into age(id, "group") values (6,'46-99');

insert into questions(id, text) values(5,'Minek a hat�s�ra t�r ki a forradalom Pesten') ;
insert into questions(id, text) values(6,'Az orsz�ggy�l�s melyik kamar�ja k�slekedik a reform javaslat elfogad�s�val?') ;
insert into questions(id, text) values(7,'H�nyadik�n fogadt�k el az �j javaslatot?') ;
insert into questions(id, text) values(8,'L�tezett-e m�r a teljes lista a 12 pontr�l m�rcius 15-�n?') ;

insert into questions(id, text) values(9,'Hol volt az els� forradalom?') ;
insert into questions(id, text) values(10,'Hol szavalta el Pet�fi a Nemzeti dalt?') ;
insert into questions(id, text) values(11,'Kit szabad�tottak ki cenz�ra megsz�ntet�se ut�n?') ;
insert into questions(id, text) values(12,'Mit j�tszottak el este a sz�nh�zban?') ;

insert into questions(id, text) values(13,'Hogy h�vt�k volna a horv�t k�vetel�sben eml�tett orsz�got?') ;
insert into questions(id, text) values(14,'H�nyadik�n l�pte �t a Dr�v�t Jellasics?') ;
insert into questions(id, text) values(15,'Ki vezette a magyar sereget a horv�t t�mad�s sor�n?') ;
insert into questions(id, text) values(16,'Hol m�rtek d�nt� veres�get a horv�tokra a magyar csapatok?') ;

insert into options(text, question_id, correct, option_id) values('P�riszban',5, true, 0);
insert into options(text, question_id, correct, option_id) values('B�csben',5, false, 1);
insert into options(text, question_id, correct, option_id) values('Pozsonyban',5, false, 2);
insert into options(text, question_id, correct, option_id) values('Pesten',5, false, 3);

insert into options(text, question_id, correct, option_id) values('Als� h�z',6, false, 0);
insert into options(text, question_id, correct, option_id) values('Fels� h�z',6, true, 1);
insert into options(text, question_id, correct, option_id) values('Mindkett�',6, false, 2);
insert into options(text, question_id, correct, option_id) values('Egyik se',6, false, 3);

insert into options(text, question_id, correct, option_id) values('M�rcius 1-�n',7, false, 0);
insert into options(text, question_id, correct, option_id) values('M�rcius 13-�n',7, false, 1);
insert into options(text, question_id, correct, option_id) values('M�rcius 14-�n',7, true, 2);
insert into options(text, question_id, correct, option_id) values('M�rcius 15-�n',7, false, 3);

insert into options(text, question_id, correct, option_id) values('Igen',8, true, 0);
insert into options(text, question_id, correct, option_id) values('Nem',8, false, 1);
insert into options(text, question_id, correct, option_id) values('Csak egy r�sze',8, false, 2);
insert into options(text, question_id, correct, option_id) values('Nem lehet tudni',8, false, 3);

insert into options(text, question_id, correct, option_id) values('A b�csi forradalom h�r�re',9, true, 0);
insert into options(text, question_id, correct, option_id) values('Mert Kossuth Lajos azt �zente',9, false, 1);
insert into options(text, question_id, correct, option_id) values('A radik�lisok akarat�ra',9, false, 2);
insert into options(text, question_id, correct, option_id) values('K�lf�ldi elnyom�s miatt',9, false, 3);

insert into options(text, question_id, correct, option_id) values('Nemzeti M�zeumn�l',10, false, 0);
insert into options(text, question_id, correct, option_id) values('Nyomd�n�l',10, true, 1);
insert into options(text, question_id, correct, option_id) values('Pilvax k�v�h�zban',10, false, 2);
insert into options(text, question_id, correct, option_id) values('Sz�nh�zban',10, false, 3);

insert into options(text, question_id, correct, option_id) values('Vilmost',11, false, 0);
insert into options(text, question_id, correct, option_id) values('K�rolyt',11, false, 1);
insert into options(text, question_id, correct, option_id) values('T�ncsicsot',11, true, 2);
insert into options(text, question_id, correct, option_id) values('Pet�fit',11, false, 3);

insert into options(text, question_id, correct, option_id) values('V. L�szl�-t',12, true, 0);
insert into options(text, question_id, correct, option_id) values('R�me� �s J�li�t',12, false, 1);
insert into options(text, question_id, correct, option_id) values('B�nk b�nt',12, false, 2);
insert into options(text, question_id, correct, option_id) values('Hamlet-et',12, false, 3);

insert into options(text, question_id, correct, option_id) values('Jugoszl�via',13, false, 0);
insert into options(text, question_id, correct, option_id) values('Dalm�cia',13, false, 1);
insert into options(text, question_id, correct, option_id) values('Illyria',13, true, 2);
insert into options(text, question_id, correct, option_id) values('Szlav�nia',13, false, 3);

insert into options(text, question_id, correct, option_id) values('Szeptember 10',14, false, 0);
insert into options(text, question_id, correct, option_id) values('Szeptember 11',14, true, 1);
insert into options(text, question_id, correct, option_id) values('Szeptember 29',14, false, 2);
insert into options(text, question_id, correct, option_id) values('Szeptember 30',14, false, 3);

insert into options(text, question_id, correct, option_id) values('M�ga J�nos',15, true, 0);
insert into options(text, question_id, correct, option_id) values('Perczel M�r',15, false, 1);
insert into options(text, question_id, correct, option_id) values('G�rgey Art�r',15, false, 2);
insert into options(text, question_id, correct, option_id) values('Teleki �d�m',15, false, 3);

insert into options(text, question_id, correct, option_id) values('Dr�v�n�l',16, false, 0);
insert into options(text, question_id, correct, option_id) values('P�kozdon',16, false, 1);
insert into options(text, question_id, correct, option_id) values('Pesten',16, false, 2);
insert into options(text, question_id, correct, option_id) values('Ozor�n�l',16, true, 3);

create or replace function create_user(nickname character varying(40), age_id integer)
returns int
language plpgsql
as
$$
declare 
	rnd int;
begin 
	rnd = floor(random() * 8999999 + 1000000);
	insert into users(nickname, created_at, age_id, user_number)
	values (nickname, now(), age_id, rnd);	
	return rnd;
end;$$;

CREATE OR replace FUNCTION validate_user(userID integer)
RETURNS bool
LANGUAGE plpgsql
AS
$$
DECLARE
	val int;
BEGIN
	select count(*) into val from users where user_number = userID;
	if val > 0 then
		return true;
	else
		return false;
	end if;
END;$$;

create or replace function create_questionnaire(userNumber integer, questionID integer, optionKey integer)
returns bool
language plpgsql
as $$
declare 
	userID int;
	optionID int;
begin 
	select id into userID from users where user_number = usernumber;
	select id into optionID from options where question_id = questionID and option_id = optionKey;
	insert into questionnaire(taken_at,user_id,question_id,option_id)
	values(now(),userID,questionID,optionID);
	return true;
end;$$;

create or replace function get_global_scores(questionIDs int[])
returns table(
	q_text varchar,
	q_right bigint,
	q_wrong bigint
)
language plpgsql
as $$
declare 
	i int[];
begin
	foreach i slice 1 in array questionIDs
		loop
			raise notice 'current value (%)', i;
			return query select q."text", 
			(select count(qu.id)
			from questionnaire qu 
			inner join "options" o on o.id = qu.option_id 
			inner join questions q on qu.question_id = q.id 
			where qu.question_id = i[1] and o.correct is true) as right,
			(select count(qu.id)
			from questionnaire qu 
			inner join "options" o on o.id = qu.option_id 
			inner join questions q on qu.question_id = q.id 
			where qu.question_id = i[1] and o.correct is false) as wrong
			from questions q where q.id = i[1];
		END LOOP;
end;$$;
--select * from get_global_scores(array[[1],[2],[3]]);

create or replace function get_scores_by_user(userID int)
returns table(
	s_time timestamp,
	s_question varchar,
	s_answer varchar,
	s_correct bool
)
language plpgsql
as $$
begin
	return query select qu.taken_at, q."text", o."text", o.correct 
	from questionnaire as qu 
	inner join questions as q on qu.question_id = q.id 
	inner join "options" as o on qu.option_id = o.id
	inner join users as u on u.id = qu.user_id 
	where u.user_number = userID order by qu.taken_at desc;
end;$$;


create or replace function add_suggestion(userID int, description character varying(300))
returns bool
language plpgsql
as $$
begin 
	insert into suggestions(taken_at,user_id ,"text")
	values(now(),(select id from users where user_number = userID), description);
	return true;
end;$$;








